module.exports = {
  css: {
    loaderOptions: {
      sass: {
        data: `@import "~@/designs/main.scss"`,
        impamentation: require('sass'),
        fiber: require('fibers')
      }
    }
  }
}
