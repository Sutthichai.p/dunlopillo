'use strict'

const os = require('os')
const path = require('path')
// const sharp = require('sharp')
const fs = require('fs-extra')
const mkdirp = require('mkdirp-promise')
const spawn = require('child-process-promise').spawn
const admin = require('firebase-admin')
const functions = require('firebase-functions')
// const { Storage } = require('@google-cloud/storage')
// const gm = require('gm').subClass({ imageMagick: true })

admin.initializeApp()

// const gcs = new Storage()
const THUMB_PREFIX = 'thumb_'

exports.generateThumbs = functions.runWith({ memory: '2GB' }).storage
  .object()
  .onFinalize(async object => {
    const filePath = object.name
    const contentType = object.contentType // This is the image MIME type
    const fileDir = path.dirname(filePath)
    const fileName = path.basename(filePath)
    const waterMarkPath = 'rawPic/frame.png'
    const thumbFilePath = path.normalize(path.join(filePath.replace('rawPic', 'thumbNail')))
    // const thumbFilePath = path.normalize(path.join('waterMark/', `${THUMB_PREFIX}${fileName}`))

    const tempLocalFile = path.join(os.tmpdir(), filePath)
    const tempWaterMarkFile = path.join(os.tmpdir(), `${fileDir}/frame.png`)
    const tempLocalThumbFile = path.join(os.tmpdir(), `${fileDir}/${THUMB_PREFIX}${fileName}`)
    const tempLocalDir = path.dirname(tempLocalFile)

    // Exit if this is triggered on a file that is not an image.
    if (filePath.includes('thumbNail/') || !contentType.includes('image')) {
      console.log('exiting function')
      return false
    }
    // Exit if the image is already a thumbnail.
    if (fileName.startsWith(THUMB_PREFIX)) {
      return console.log('Already a Thumbnail.')
    }
    let size, crop
    if (object.contentType === 'image/jpeg') {
      size = '512x512^'
      crop = '512x400+0+0'
    }

    const bucket = admin.storage().bucket(object.bucket)
    const file = bucket.file(filePath)
    const waterMarkFile = bucket.file(waterMarkPath)
    try {
      // Create the temp directory where the storage file will be downloaded.
      await mkdirp(tempLocalDir)
      // Download file from bucket.
      await file.download({ destination: tempLocalFile })
      console.log('The file has been downloaded to', tempLocalFile)

      await waterMarkFile.download({ destination: tempWaterMarkFile })
      console.log('waterMarkFile has been downloaded to', tempWaterMarkFile)

      // await spawn('convert', [tempLocalFile, '-resize', '512x512>', '-strip', tempWaterMarkFile, '-gravity', 'southeast ', '-composite', tempLocalThumbFile])
      await spawn('convert', [tempLocalFile, '-geometry', size, '-gravity', 'center', '-crop', crop, tempWaterMarkFile, '-gravity', 'southeast ', '-composite', tempLocalThumbFile])

      await bucket.upload(tempLocalThumbFile, { destination: thumbFilePath })
    } catch (err) {
      console.log(err)
    }
    // Once the image has been uploaded delete the local files to free up disk space.
    fs.unlinkSync(tempLocalFile)
    fs.unlinkSync(tempLocalThumbFile)
    fs.unlinkSync(tempWaterMarkFile)

    return console.log('The end')
  })

exports.countPicInUserRegister = functions.firestore
  .document('usersRegister/{userId}/userUploadPicTransactions/{pictureId}')
  .onWrite((change, context) => {
    const id = context.params.userId

    const docRef = admin.firestore()
      .collection('usersRegister')
      .doc(id)

    return docRef.collection('userUploadPicTransactions').orderBy('createAt', 'desc').get()
      .then(querySnapshort => {
        const picCount = querySnapshort.size
        let allPic = []
        let lastTransactions = []

        querySnapshort.forEach(element => {
          allPic.push(element.data())
        })

        lastTransactions = allPic.slice(0, 2)

        let data = {
          picCount,
          lastTransactions
        }
        return docRef.update(data)
      })
      .catch(err => console.log(err))
  })
