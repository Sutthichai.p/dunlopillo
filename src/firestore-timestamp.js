import firebase from '@firebase/app'
import 'firebase/firestore'

export const getServerTimestamp = () => {
  return firebase.firestore.FieldValue.serverTimestamp()
}
