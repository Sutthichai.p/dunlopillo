const app = {
  namespaced: true,
  state: {
    loading: false,
    snackbar: false,
    textSnackbar: '',
    colorStatusSnackbar: ''
  },
  mutations: {
    setLoading (state, payload) {
      state.loading = payload
    },
    setsnackbar (state, payload) {
      state.snackbar = payload
    },
    settextSnackbar (state, payload) {
      state.textSnackbar = payload
    },
    setcolorStatusSnackbar (state, payload) {
      state.colorStatusSnackbar = payload
    }
  },
  actions: {
    runLoader ({ commit }, payload) {
      commit('setLoading', payload)
    },
    runErrorSnackbar ({ commit }, { status, text }) {
      commit('setsnackbar', status)
      commit('settextSnackbar', text)
      commit('setcolorStatusSnackbar', 'error')
    },
    runSuccessSnackbar ({ commit }, { status, text }) {
      commit('setsnackbar', status)
      commit('settextSnackbar', text)
      commit('setcolorStatusSnackbar', 'green')
    },
    closeSnackbar ({ commit }) {
      commit('setsnackbar', false)
    },
    closeloading ({ commit }) {
      commit('setLoading', false)
    }
  },
  getters: {
    getLoading (state) {
      return state.loading
    },
    getSnackbar (state) {
      return state.snackbar
    },
    gettextSnackbar (state) {
      return state.textSnackbar
    },
    getcolorStatusSnackbar (state) {
      return state.colorStatusSnackbar
    }
  }
}

export default app
