import { firebaseApp } from '@/firebase.init.js'
import { getServerTimestamp } from '@/firestore-timestamp'
import { fireStoreApp } from '@/firestore.init'
import router from '@/router'

const storageApp = firebaseApp.storage().ref()

const pictures = {
  namespaced: true,
  state: {
    picturesUrl: null,
    picId: null,
    picObj: {}
  },
  mutations: {
    setPicturesUrl (state, patload) {
      state.picturesUrl = patload
    },
    setPicId (state, payload) {
      state.picId = payload
    },
    setPicObj (state, payload) {
      state.picObj = payload
    }
  },
  actions: {
    async uploadToStorage ({ commit, dispatch, rootState }, picObj) {
      console.log('uploadToStorage')
      commit('setPicturesUrl', picObj)
      const { dataUrl, ...metadata } = picObj
      let dateUpload = new Date()
      console.log(metadata)
      let uuid = rootState.user.lineInfo.sub
      let ref = storageApp.child(`rawPic/${uuid}/${dateUpload}.jpg`)
      let rawPicUrl
      try {
        await ref.putString(picObj.dataUrl, 'data_url')
        rawPicUrl = await ref.getDownloadURL()
      } catch (err) {
        console.log(err)
      }
      console.log('raw :', rawPicUrl)
      if (rawPicUrl) {
        let thumbNailUrl = rawPicUrl.replace('rawPic', 'thumbNail')
        let dataUser = {
          user: rootState.user.lineInfo,
          mobileNo: rootState.user.phone,
          email: rootState.user.email,
          rawPicUrl: rawPicUrl,
          thumbNailUrl: thumbNailUrl,
          createAt: getServerTimestamp()
        }
        dispatch('saveToDB', dataUser)
      } else {
        dispatch('app/runLoader', false, { root: true })
        console.log('upload fail')
      }
    },
    async saveToDB ({ commit, dispatch }, dataUserobj) {
      console.log('saveToDB')
      let uploadCollection
      console.log(dataUserobj)
      try {
        uploadCollection = await fireStoreApp.collection('uploadPicTransactions').add(dataUserobj)
        await fireStoreApp.collection('usersRegister')
          .doc(`${dataUserobj.user.sub}`)
          .collection('userUploadPicTransactions')
          .doc(`${uploadCollection.id}`).set(dataUserobj)
      } catch (err) {
        console.log(err)
        dispatch('app/runLoader', false, { root: true })
      }

      if (uploadCollection) {
        dispatch('app/runLoader', false, { root: true })
        commit('setPicId', uploadCollection.id)
        commit('setPicObj', {})
        router.push({ name: 'uploadSuccess', params: { id: uploadCollection.id } })
        console.log('ok thank you :', uploadCollection.id)
      }
    },
    async queryPic ({ commit, state }, picId) {
      let res, picObj
      if (!state.picId) {
        commit('setPicId', picId)
      }
      try {
        res = await fireStoreApp.collection('uploadPicTransactions').doc(`${picId}`).get()
      } catch (err) {
        console.log(err)
      }
      if (res.exists) {
        picObj = res.data()
        commit('setPicObj', picObj)
      }
    }
  },
  getters: {
    getPicObjRaw (state) {
      return state.picObj.rawPicUrl
    },
    getPicObjThumb (state) {
      return state.picObj.thumbNailUrl
    }
  }
}
export default pictures
