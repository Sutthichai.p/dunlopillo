import axios from 'axios'
import jwtDecode from 'jwt-decode'
import assign from 'lodash/assign'
import router from '@/router.js'
import { getServerTimestamp } from '@/firestore-timestamp'
import { fireStoreApp } from '@/firestore.init'
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'

const User = {
  namespaced: true,
  state: {
    // token: null,
    isUserFB: false,
    lineInfo: {},
    fullName: '',
    phone: '',
    email: '',
    lastTransaction: null

  },
  mutations: {
    setIsUserFB (state, payload) {
      state.isUserFB = payload
    },
    setLineInfo (state, payload) {
      state.lineInfo = payload
    },
    setElementLineInfo (state, payload) {
      assign(state.lineInfo, payload)
    },
    setPhone (state, payload) {
      state.phone = payload
    },
    setEmail (state, payload) {
      state.email = payload
    },
    setLastTransaction (state, payload) {
      state.lastTransaction = payload
    }
  },
  actions: {
    async CallLineAPI ({ commit, state }) {
      let code = router.currentRoute.query.code
      if (code) {
        let params = new URLSearchParams()
        params.append('grant_type', 'authorization_code')
        params.append('code', `${code}`)
        params.append('redirect_uri', `https://www.dunlopilloliveyourlifetothefullest.com/register`)
        params.append('client_id', `1598476672`)
        params.append('client_secret', `0822d03d880f4853e42e2a82dc556ff5`)
        let resouce, userdata

        try {
          resouce = await axios.post('https://api.line.me/oauth2/v2.1/token', params)
          userdata = jwtDecode(resouce.data.id_token)
        } catch (err) {
          console.log(err)
        }
        if (resouce && userdata) {
          commit('setLineInfo', userdata)
        } else {
          router.push('/')
        }
      } else {
        if (!state.lineInfo.sub) {
          console.log('Come back From Rich menu')
          router.push('/')
        }
        console.log('Come back from Rule')
      }
    },
    async checkUserData ({ state }) {
      let data
      try {
        data = await fireStoreApp.collection('usersRegister').doc(`${state.lineInfo.sub}`).get()
      } catch (err) {
        console.log(err)
      }
      return data
    },
    addUsername ({ commit }, payload) {
      commit('setElementLineInfo', payload)
    },
    addPicture ({ commit }, payload) {
      commit('setElementLineInfo', payload)
    },
    addPhone ({ commit }, payload) {
      commit('setPhone', payload)
    },
    addEmail ({ commit }, payload) {
      commit('setEmail', payload)
    },
    addUserExits ({ commit }, payload) {
      commit('setIsUserFB', payload)
    },
    saveUserRegister ({ commit, state }, { phone, email }) {
      commit('setPhone', phone)
      commit('setEmail', email)
      let data = {
        fullName: state.lineInfo.name,
        email: email,
        mobileNo: phone,
        profilePic: state.lineInfo.picture,
        createAt: getServerTimestamp(),
        randomRef1: getRandomNumber(),
        randomRef2: getRandomNumber(),
        randomRef3: getRandomNumber(),
        flagSelected: false
      }
      fireStoreApp.collection('usersRegister').doc(`${state.lineInfo.sub}`)
        .set(data, { merge: true })
        .then(() => {
          commit('setIsUserFB', true)
          router.push('/thankyou')
        })
    },
    fetchLastPic ({ commit, state }) {
      return fireStoreApp.collection('usersRegister').doc(`${state.lineInfo.sub}`).get().then(query => {
        commit('setLastTransaction', query.data().lastTransactions)
      }).catch(err => console.log(err))
    }
  },
  getters: {
    getIsUserFB (state) {
      return state.isUserFB
    },
    getLineInfo (state) {
      return state.lineInfo
    },
    getElementLineInfo (state) {
      return state.lineInfo
    },
    getPhone (state) {
      return state.phone
    },
    getEmail (state) {
      return state.email
    },
    getLastTransaction (state) {
      return state.lastTransaction
    },
    checkUserAlreadyExist (state) {
      return !!state.lastTransaction
    }
  }
}

export default User

function getRandomNumber () {
  return window.crypto.getRandomValues(new Uint32Array(1))[0]
}
