import Vue from 'vue'
import Vuex from 'vuex'
import user from '@/state/user'
import app from '@/state/app'
import pictures from '@/state/pictures'
import userReport from '@/state/userReport'
import createPersistedState from 'vuex-persistedstate'
import Cookie from 'js-cookie'

Vue.use(Vuex)

export default new Vuex.Store({
  plugins: [createPersistedState({
    getItem: key => Cookie.getJSON(key),
    setItem: (key, value) => Cookie.set(key, value, { expires: 1, secure: true })
  })],
  modules: {
    user,
    app,
    pictures,
    userReport
  }
})
