import { firebaseApp } from '@/firebase.init.js'
import { fireStoreApp } from '@/firestore.init'
import router from '@/router.js'
import assign from 'lodash/assign'

const userReport = {
  namespaced: true,
  state: {
    userName: '',
    userArr: []
  },
  mutations: {
    setUserName (state, payload) {
      state.userName = payload
    },
    setUserArr (state, payload) {
      state.userArr = payload
    },
    setAddUserArr (state, { index, editObj }) {
      assign(state.userArr[index], editObj)
    }
  },
  actions: {
    login ({ state, commit, dispatch }, { email, password }) {
      firebaseApp.auth().signInWithEmailAndPassword(email, password)
        .then(firebaseUser => {
          dispatch('app/runLoader', false, { root: true })
          commit('setUserName', firebaseUser.user.email)
          console.log(firebaseUser.user.email)
          if (state.userName === 'dunlop@admin.com' || state.userName === 'admin@dunlopillothai.com') {
            router.push('/report')
          } else {
            router.push('/photoLists')
          }
        })
        .catch(error => {
          console.log(error)
          alert('Oops. ' + error.message)
        })
    },
    async getUserobjData ({ commit }) {
      const data = await fireStoreApp.collection('usersRegister').get()

      commit('setUserArr', data.docs.map(doc => {
        let id = doc.id
        let data = doc.data()
        const dataAll = { id, ...data }

        return dataAll
      }))
      return console.log('we', data.docs.map(doc => doc.data()))
    },
    editUserArr ({ commit, dispatch }, { index, editObj, id }) {
      return fireStoreApp
        .collection('usersRegister')
        .doc(`${id}`)
        .set(editObj, { merge: true })
        .then(() => {
          console.log('success')
          commit('setAddUserArr', { index: index, editObj: editObj })
          dispatch('app/runSuccessSnackbar',
            {
              status: true,
              text: 'User Edit Success'
            }, { root: true })
        })
        .catch((err) => {
          console.log(err)
          dispatch('app/runErrorSnackbar',
            {
              status: true,
              text: err
            }, { root: true })
        })
    },
    logout ({ commit }) {
      firebaseApp.auth().signOut()
        .then(() => {
          commit('setUserName', '')
          router.push('/loginReport')
        }).catch((error) => {
          console.log(error)
        })
    }
  },
  getters: {
    getUserArr (state) {
      return state.userArr
    },
    checkArr (state) {
      return !!state.userArr
    },
    checkUsername (state) {
      return !!state.userName
    },
    getUsername (state) {
      return state.userName
    }
  }
}

export default userReport
