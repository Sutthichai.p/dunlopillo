import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import '@mdi/font/css/materialdesignicons.css'
import '@fortawesome/fontawesome-free/css/all.css'

Vue.use(Vuetify)
export default new Vuetify({
  theme: {
    dark: false,
    iconfont: 'fa',
    themes: {
      light: {
        primary: '#d2bb8c',
        secondary: '#717c7d',
        accent: '#000000',
        error: '#ff0000',
        warning: '#ffff00',
        info: '#e91e63',
        success: '#f44336'
      }
    }
  },
  options: {
    customProperties: true
  }
})
