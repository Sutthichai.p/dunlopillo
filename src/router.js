import Vue from 'vue'
import Router from 'vue-router'
import store from './state/store.js'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    { path: '/', name: 'login-line', component: () => import('./views/login-line.vue') },
    { path: '/register', name: 'register', component: () => import('./views/register.vue') },
    { path: '/rule', name: 'rule', component: () => import('./views/rule.vue'), meta: { requiresAuth: true } },
    { path: '/upload', name: 'upload', component: () => import('./views/upload.vue'), meta: { requiresAuth: true } },
    { path: '/rulepublic', name: 'rulepublic', component: () => import('./views/rulePubic.vue') },
    { path: '/announce', name: 'announce', component: () => import('./views/announce.vue') },
    { path: '/thankyou', name: 'thankyou', component: () => import('./views/thankyou.vue'), meta: { requiresAuth: true } },
    { path: '/uploadSuccess/:id', name: 'uploadSuccess', component: () => import('./views/uploadSuccess.vue') },
    { path: '/uploadSuccess', redirect: { name: '404' } },
    { path: '*', redirect: { name: '404' } },
    { path: '/404', name: '404', component: () => import('./views/404.vue') },

    { path: '/loginReport', name: 'login-report', component: () => import('./views/login-report.vue') },
    { path: '/photoLists', name: 'photolist', component: () => import('./views/photolist.vue'), meta: { requiresAuthReport: true } },
    { path: '/finalPhotoLists', name: 'FinalPhotoList', component: () => import('./views/FinalPhotoList.vue'), meta: { requiresAuthReport: true } },
    { path: '/report', name: 'report', component: () => import('./views/report.vue'), meta: { requiresAuthReport: true } }
  ]
})

router.beforeEach((to, from, next) => {
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth)
  const requiresAuthReport = to.matched.some(record => record.meta.requiresAuthReport)
  if (requiresAuth) {
    if (to.name === 'rule') {
      next()
    } else {
      (store.getters['user/getIsUserFB'])
        ? next()
        : next({ path: '/' })
    }
  }
  if (requiresAuthReport) {
    const checkUser = store.getters['userReport/checkUsername']
    const getUser = store.getters['userReport/getUsername']
    console.log(checkUser)
    console.log(getUser)
    console.log(to.fullPath)
    if (checkUser) {
      switch (getUser) {
        case 'dunlop@admin.com': next()
          break
        case 'admin@dunlopillothai.com': next()
          break
        case 'committee01@dunlopillothai.com': next()
          break
        case 'committee02@dunlopillothai.com': next()
          break
        case 'committee03@dunlopillothai.com': next()
          break
      }
      // if (getUser === 'dunlop@admin.com' || getUser === 'admin@dunlopillothai.com') {
      //   next()
      // } else {
      //   next({ path: '/photoList' })
      // }
    } else {
      next({ path: '/loginReport' })
    }
  }

  console.log('beforeEach')
  next()
})

export default router
