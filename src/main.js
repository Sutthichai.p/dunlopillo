import Vue from 'vue'
import vuetify from './plugins/vuetify'
import App from './App.vue'
import router from './router'
import store from './state/store'
import './plugins/uploadAndResize'
import './plugins/vue-social-sharing'
import './plugins/json-excel'

Vue.config.productionTip = false

new Vue({
  vuetify,
  router,
  store,
  render: h => h(App)
}).$mount('#app')
