import { firebaseConfig } from '@/config.js'
import firebase from '@firebase/app'
import '@firebase/auth'
import 'firebase/firestore'
import 'firebase/storage'

export const firebaseApp = firebase.initializeApp(firebaseConfig)
